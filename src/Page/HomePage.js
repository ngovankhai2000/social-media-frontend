import React from "react"
import LeftHome from "../Components/HomeScreen/HomePage/LeftHome"
import CenterHome from "../Components/HomeScreen/HomePage/CenterHome"
import RightHome from "../Components/HomeScreen/HomePage/RightHome"
function HomePage() {
    return (
        <div className="HomePage">
            <div className="leftHomePage">
                <LeftHome> </LeftHome>
            </div>
            <div className="centerHomePage">
                <CenterHome></CenterHome>
            </div>
            <div className="rightHomePage">
                <RightHome> </RightHome>
            </div>

        </div>
    )
}

export default HomePage