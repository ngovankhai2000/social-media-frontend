import React from 'react';
import { BrowserRouter, Route, Link } from "react-router-dom"
import "./Component.css"
import LoginScreen from './Screens/LoginScreeen';
import HomeScreen from './Screens/HomeScreen';


function App() {
  return (
    <BrowserRouter>
      <Route path="/login" exact component={LoginScreen} />
      <Route path="/" exact component={HomeScreen} />
    </BrowserRouter>

  );
}

export default App;
