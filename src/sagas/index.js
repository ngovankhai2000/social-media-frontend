import * as api from "../api/allApi"
import * as postTypes from "../types/postTypes"
import * as loginTypes from "../types/loginTypes"

import { call, cancel, cancelled, fork, put, take, takeEvery } from 'redux-saga/effects';

export function* authorize(email, password) {
  try {
    const data = yield call(api.login, { email, password })
    const token = data.token
    yield put({ type: loginTypes.LOGIN_SUCCESS })
    yield put({ type: loginTypes.SAVE_TOKEN, token });
  } catch (error) {
    yield put({ type: loginTypes.LOGIN_ERROR, error })
  }
  finally {
    if (yield cancelled()) {
      yield put({ type: loginTypes.LOGIN_CANCELLED })
    }
  }
}
export function* post(action) {
  let { content, name, userID } = action;
  try {
    const data = yield call(api.post, { content, name, userID })
    yield put({ type: postTypes.POST_SUCCESS })
  } catch (error) {
    yield put({ type: postTypes.POST_ERROR, error })
  }
}
export function* listPost(action) {
  try {
    const data = yield call(api.listPost)
    console.log(data)
    yield put({ type: postTypes.LIST_POST_SUCCESS, data })
  } catch (error) {
    yield put({ type: postTypes.LIST_POST_ERROR, error })
  }
}
export function* watchPost() {
  yield takeEvery(postTypes.POST_REQUEST, post)
  yield takeEvery(postTypes.LIST_POST_REQUEST, listPost)

}


export function* loginFlow() {
  while (true) {
    const { email, password } = yield take('LOGIN_REQUEST')
    const task = yield fork(authorize, email, password)
    const action = yield take(['LOGOUT', 'LOGIN_ERROR'])
    if (action.type === 'LOGOUT') {
      yield cancel(task)
      yield put({ type: 'DELETE_TOKEN' })
    }
  }
}

export function* logActions() {
  while (true) {
    const action = yield take('*')
    console.log(action.type);
  }
}
