import * as types from '../types/postTypes';
export const post = (content, name, userID) => {
    return { type: types.POST_REQUEST, content: content, name: name, userID: userID }
}
export const requestListPost = () => {
    return { type: types.LIST_POST_REQUEST }
}