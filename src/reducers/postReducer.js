import * as actionTypes from '../types/postTypes'
import { LOGGED_IN, LOGGED_OUT, LOGIN_CANCELLED, LOGIN_ERROR } from '../types/statusTypes';
import initialState from '../initialState';

export default function userRole(state = initialState.post, action) {
    let newState;
    switch (action.type) {
        case actionTypes.POST_SUCCESS:
            newState = { ...state, status: "post-sucess" };
            return newState;
        case actionTypes.POST_ERROR:
            newState = { ...state, status: "post-error" };
            return newState;
        case actionTypes.LIST_POST_SUCCESS:
            newState = { listPost: [...state.listPost, ...action.data] };
            return newState;
        case actionTypes.LIST_POST_ERROR:
            newState = { ...state, status: "post-error" };
            return newState;
        default:
            return state;
    }
}
