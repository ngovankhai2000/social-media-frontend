import { combineReducers } from 'redux';
import login from './loginReducer';
import post from "./postReducer"

const rootReducer = combineReducers({
    login,
    post
});

export default rootReducer;
