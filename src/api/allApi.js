import axios from "axios"
import { URL } from "../configSys"
import { useSelector } from "react-redux";
// import { LOGIN_REQUEST } from "../types/loginTypes"

export const login = async ({ email, password }) => {
    const { data } = await axios.post("http://localhost:3001/user/signin", { email, password })

    return data;
}
export const post = async ({ content, name, userID }) => {
    const data = await axios.post("http://localhost:3001/post", { content, name, userID })
    return data;
}
export const listPost = async () => {

    const { data } = await axios.get("http://localhost:3001/post/5")


    return data;
}