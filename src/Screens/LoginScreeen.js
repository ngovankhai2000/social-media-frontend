import React, { useState, useEffect } from "react"
import Account from "../Components/LoginScreen/Account"
import "../Login.css"
import { useDispatch } from "react-redux";
import { signin } from "../actions/loginActions";

function LoginScreen() {
    const dispatch = useDispatch();
    const [emailERR, setEmailERR] = useState("");
    const [passwordERR, setPasswordERR] = useState("");
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    useEffect(() => {
        if (email != "") validate(email);
        if (password != "") setPasswordERR("")
    }, [email, password])
    const submitHandler = (e) => {
        e.preventDefault();
        // console.log(emailERR, "ddd ", passwordERR)
        if (checkCanSubmit()) { dispatch(signin(email, password)); console.log("submit") }
    }
    function validate() {
        if (validateEmail(email)) {
            setEmailERR("");
        } else {
            setEmailERR("Wrong email format")
        }
    }
    function checkCanSubmit() {
        if (email == "") { setEmailERR(" Please enter email !") }
        if (password == "") setPasswordERR(" Please enter password ! ")
        if (email == "" || password == "") return false
        if (emailERR != "" || passwordERR != "") return false;
        return true
    }
    return (
        <div className="loginScreen">
            <div className="loginRecently">
                <img className="logoFB" src="https://static.xx.fbcdn.net/rsrc.php/y8/r/dF5SId3UHWd.svg" />
                <div style={{ fontSize: "1.75rem", fontWeight: "bold" }}> Login recently</div>
                <div> Click into your image or add account</div>
                <div className="wrapAccounts">
                    <Account image="https://via.placeholder.com/160" name="Khai Ngô" />
                    <Account image="https://via.placeholder.com/160" name="Khai Ngô" />
                    <Account image="https://via.placeholder.com/160" name="Khai Ngô" />
                    <Account image="https://via.placeholder.com/160" name="Khai Ngô" />
                </div>
            </div>
            <div className="loginForm">
                <input className={emailERR == "" ? "textInput" : "textInPutERR"} type="text" placeholder="Email or number phone" onChange={(e) => setEmail(e.target.value)}></input>
                <div style={{ display: emailERR == "" ? "none" : "block" }} className="errString">{emailERR}</div>
                <input className="textInput" type="text" placeholder="Password" onChange={(e) => setPassword(e.target.value)}></input>
                <div style={{ display: passwordERR == "" ? "none" : "block" }} className="errString">{passwordERR}</div>
                <button className="submitButton" type="submit" onClick={submitHandler}>Login</button>
                <a href="https://www.facebook.com/" >Forgotten acount ?</a>
                <div className="horizontalLine"></div>
                <button className="createButton" >Create a new account</button>
            </div>
        </div >
    )
}

export default LoginScreen