import React from "react"
import HomePage from "../Page/HomePage"
import Navbar from "../Components/HomeScreen/Navbar"

function HomeScreen() {
    return (
        <div>
            <Navbar />
            <HomePage />
        </div>
    )
}

export default HomeScreen