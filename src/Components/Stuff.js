import React from "react"
import { icon } from "@fortawesome/fontawesome-svg-core"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

function Stuff(props) {
    const { icon, text, classN = "stuff" } = props
    return (
        <div className={classN} >
            <FontAwesomeIcon icon={icon} />
            <div className="textProfile">{text}</div>
        </div>
    )

}

export default Stuff