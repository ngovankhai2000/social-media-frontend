import React from "react"

function ImageProfile(props) {
    let { image } = props;
    return (
        <div className="profileImage" style={{ backgroundImage: `url(${image})` }} ></div>
    )
}

export default ImageProfile