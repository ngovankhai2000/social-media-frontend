import React from "react"
import ImageProfile from "./ImageProfile"


function StuffProfile(props) {
    const { img, userName } = props
    return (
        <div className="stuff">
            <ImageProfile image={img} />
            <div className="textProfile">{userName}</div>
        </div>
    )

}

export default StuffProfile