import React from 'react'
import "./Navbar.css"
import { faHome, faPager, faTv, faBuilding, faUser, faUserCircle, faPlus, faBell, faCaretDown, faSea, faSearch } from "@fortawesome/free-solid-svg-icons";
import { faFacebook, faFacebookMessenger } from "@fortawesome/free-brands-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import StuffProfile from "../StuffProfile"

function Navbar() {
    let Background = "https://via.placeholder.com/150 "
    return (
        <div className="container">
            <div className='leftNavbar'>
                <div className="wrapContentLeftNavbar">
                    <FontAwesomeIcon className="paddingIcon iconFaceBook" icon={faFacebook} />
                    <div className="searchBar">
                        <FontAwesomeIcon className="searchIcon" icon={faSearch} />
                        <input className="seachInput" type="text" placeholder="Search FaceBook" />
                    </div>
                </div>
            </div>
            <div className="centerNavbar">
                <div class="wrapCenterIcon">
                    <FontAwesomeIcon className="paddingIconSquare" icon={faHome} />
                    <FontAwesomeIcon className="paddingIconSquare" icon={faPager} />
                    <FontAwesomeIcon className="paddingIconSquare" icon={faTv} />
                    <FontAwesomeIcon className="paddingIconSquare" icon={faBuilding} />
                    <FontAwesomeIcon className="paddingIconSquare" icon={faUser} />
                </div>
            </div>
            <div className="rightNavbar">
                <div className="wrapContentRightNavbar">
                    {/* <div className="stuff">
                        <div className="profileImage" style={{ backgroundImage: `url(${Background})` }} ></div>
                        <div className="textProfile">Name</div>
                    </div> */}
                    <StuffProfile img={Background} userName={"khai ngo"} />
                    <FontAwesomeIcon className="paddingIconCircle" icon={faPlus} />
                    <FontAwesomeIcon className="paddingIconCircle" icon={faFacebookMessenger} />
                    <FontAwesomeIcon className="paddingIconCircle" icon={faBell} />
                    <FontAwesomeIcon className="paddingIconCircle" icon={faCaretDown} />
                </div>

            </div>
        </div>
    )
}

export default Navbar