import React, { useState } from "react"
import StuffProfile from "../../StuffProfile"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons"

function CreatePost({ showCreatePost = false }) {
    console.log(showCreatePost, "ddd")
    let [show, setShow] = useState(showCreatePost)
    let [context, setContext] = useState("")
    console.log(context, "dkkdkdkd")
    return (
        <div className="overlay" style={{ display: show ? 'block' : 'none' }} >
            <div className="createPost" >
                <button className="exitCreatePost" onClick={() => { setShow(false) }} >
                    <FontAwesomeIcon icon={faTimes} />
                </button>
                <div > Create Post</div>
                <br></br>
                <StuffProfile img="https://via.placeholder.com/150" userName="Khai"></StuffProfile>
                <input className="postContent noBoder noOutlineFocus  " placeholder="What's on your mind"
                    value={context} onChange={(e) => { setContext(e.target.value); console.log(e.target.value) }}></input>
                <button className={context == "" ? "postButtonUnEnable" : "postButton"}>Post</button>
            </div>
        </div>
    )
}

export default CreatePost