import React, { useState, useEffect } from "react"
import Picker from 'emoji-picker-react';
import ImageProfile from "../../ImageProfile"
import Stuff from "../../Stuff"
import { faVideo, faPhotoVideo, faSmile, faTimes } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import CreatePost from "./CreatePost"
import StuffProfile from "../../StuffProfile"
import EmojiPicker from "emoji-picker-react";
import { useDispatch } from "react-redux";
import { post } from "./../../../actions/postActions"

function PreCreatePost() {
    const dispatch = useDispatch()
    const [chosenEmoji, setChosenEmoji] = useState(null);
    const [emojiPanel, setEmojiPanel] = useState(false)

    const [showCreatePost, setShowCreatePost] = useState(false);
    const [content, serContent] = useState("")
    useEffect(() => {
        if (chosenEmoji != null)
            serContent(content + chosenEmoji.emoji)
    }, [chosenEmoji])
    const onEmojiClick = (event, emojiObject) => {
        setChosenEmoji(emojiObject);

    };
    let name = "khai";
    let userID = "123"
    const submitPost = () => {
        setShowCreatePost(!showCreatePost)
        dispatch(post(content, name, userID))
    }
    return (
        <div className="post">
            <div className="wrapFlex" >
                <ImageProfile image="https://via.placeholder.com/150" />
                <button className="buttonCreatePost" onClick={() => setShowCreatePost(true)} > What's on your mind?</button>
            </div>
            <hr></hr>
            <div className="wrapFlex">
                <Stuff icon={faVideo} text="Live Video" />
                <Stuff icon={faPhotoVideo} text="Photo/Video" />
                <Stuff icon={faSmile} text="Feeling/Activity" />
            </div>
            <div className="overlay" style={{ display: showCreatePost ? 'flex ' : 'none' }} >
                <div className="createPost" >
                    <button className="exitCreatePost" onClick={() => { setShowCreatePost(false) }} >
                        <FontAwesomeIcon icon={faTimes} />
                    </button>
                    <div > Create Post</div>
                    <br></br>
                    <StuffProfile img="https://via.placeholder.com/150" userName="Khai"></StuffProfile>
                    <textarea className="postContent noBoder noOutlineFocus  " placeholder="What's on your mind"
                        onClick={() => setEmojiPanel(false)} value={content} onChange={(e) => serContent(e.target.value)}></textarea>
                    <FontAwesomeIcon className="emojiIcon" icon={faSmile} onClick={() => setEmojiPanel(!emojiPanel)} />
                    <div className="emojiPanel" style={{ display: emojiPanel ? "block" : "none" }}>
                        <Picker onEmojiClick={onEmojiClick} />
                    </div>
                    <button className={content == "" ? "postButtonUnEnable" : "postButton"} onClick={submitPost}>Post</button>
                </div>
            </div>
        </div>
    )
}

export default PreCreatePost