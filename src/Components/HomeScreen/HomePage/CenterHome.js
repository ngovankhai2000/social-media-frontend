import React, { useEffect } from "react"
import Post from "./Post"
import PreCreatePost from "./PreCreatePost"
import CreatePost from "./CreatePost"
import { useSelector, useDispatch } from "react-redux"
import { requestListPost } from "../../../actions/postActions"
import InfiniteScroll from 'react-infinite-scroll-component';
function CenterHome() {
    let Background = "https://via.placeholder.com/150"
    let userName = "khai"
    const post = useSelector(state => state.post)
    let { listPost } = post;
    const fetchData = () => {
        dispatch(requestListPost())
    }
    let dispatch = useDispatch();
    useEffect(() => {
        dispatch(requestListPost());
        return () => {
        }
    }, [])

    return (
        <div>
            <PreCreatePost />
            <InfiniteScroll
                dataLength={listPost.length} //This is important field to render the next data
                next={fetchData}
                hasMore={true}
                loader={<h4>Loading...</h4>}
            >
                <div class="containerPost">
                    {
                        listPost.map(post => (
                            <Post data={post} />
                        ))
                    }
                </div>
            </InfiniteScroll >


        </div >
    )
}

export default CenterHome