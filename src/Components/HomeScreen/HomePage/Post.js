import React, { useState } from "react"
import { faThumbsUp, faComment, faShare, faEllipsisH } from '@fortawesome/free-solid-svg-icons'

import StuffProfile from "../../StuffProfile"
import Stuff from "../../Stuff"

function Post(props) {
    const [liked, setLiked] = useState(false);
    console.log(liked, "kkkkkkkkkkkkkk")
    let Background = "https://via.placeholder.com/150"
    let userName = props.data.name;
    let text = props.data.content;
    return (
        <div class="post">
            <div className="topPost">
                <StuffProfile img={Background} userName={userName} />
                <Stuff icon={faEllipsisH} text="" />
            </div>
            <div className="middlePost">
                {text}
            </div>
            {/* <div>
                {imagePost}
            </div> */}
            <div className="bottomPost"  >
                <div className="like-group" onClick={() => { setLiked(!liked) }}>
                    <Stuff icon={faThumbsUp} classN={liked ? "activeThumbUps" : "thumbUps"} />
                    <span className={liked ? "activeLike" : "thumbUps"}>Like</span>
                </div>
                <Stuff icon={faComment} text="Comment" />
                <Stuff icon={faShare} text="Share" />

            </div>
        </div>
    )

}

export default Post