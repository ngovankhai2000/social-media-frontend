import React from "react"
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

function Account(props) {
    let { image, name } = props
    return (
        <div className="account">
            <FontAwesomeIcon className="deleteAccount" icon={faTimesCircle} />
            <div className="backgroundImage" style={{ backgroundImage: `url(${image})` }} />
            {/* <img src={props.image}></img> */}
            <p>{props.name}</p>
        </div >
    )
}

export default Account